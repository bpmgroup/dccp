package fr.bpifrance.son.dccp.model;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class CadeauModel implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("Expéditeur")
	private java.lang.String expediteur;
	@org.kie.api.definition.type.Label("Date de réception")
	private java.util.Date receptionDate;
	@org.kie.api.definition.type.Label("Traitement")
	private java.lang.String traitement;
	@org.kie.api.definition.type.Label("Valeur estimée(en €)")
	private java.lang.Double valeurEstimee;
	@org.kie.api.definition.type.Label("Lien d'affaires")
	private java.lang.String lien;
	@org.kie.api.definition.type.Label("Description détaillée")
	private java.lang.String description;
	@org.kie.api.definition.type.Label("Commentaires")
	private java.lang.String commentaire;

	@org.kie.api.definition.type.Label(value = "Nom Manager")
	private java.lang.String managerName;

	@org.kie.api.definition.type.Label(value = "Nom")
	private java.lang.String nom;

	@org.kie.api.definition.type.Label(value = "Prénom")
	private java.lang.String prenom;

	@org.kie.api.definition.type.Label(value = "Fonction")
	private java.lang.String fonction;

	@org.kie.api.definition.type.Label(value = "Service")
	private java.lang.String service;

	@org.kie.api.definition.type.Label(value = "Entité juridique")
	private java.lang.String entiteJurid;

	@org.kie.api.definition.type.Label(value = "Pour le compte de (Nom, prénom de la personne)")
	private java.lang.String compteDeNom;

	public CadeauModel() {
	}

	public java.lang.String getExpediteur() {
		return this.expediteur;
	}

	public void setExpediteur(java.lang.String expediteur) {
		this.expediteur = expediteur;
	}

	public java.util.Date getReceptionDate() {
		return this.receptionDate;
	}

	public void setReceptionDate(java.util.Date receptionDate) {
		this.receptionDate = receptionDate;
	}

	public java.lang.String getTraitement() {
		return this.traitement;
	}

	public void setTraitement(java.lang.String traitement) {
		this.traitement = traitement;
	}

	public java.lang.Double getValeurEstimee() {
		return this.valeurEstimee;
	}

	public void setValeurEstimee(java.lang.Double valeurEstimee) {
		this.valeurEstimee = valeurEstimee;
	}

	public java.lang.String getLien() {
		return this.lien;
	}

	public void setLien(java.lang.String lien) {
		this.lien = lien;
	}

	public java.lang.String getDescription() {
		return this.description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public java.lang.String getCommentaire() {
		return this.commentaire;
	}

	public void setCommentaire(java.lang.String commentaire) {
		this.commentaire = commentaire;
	}

	public java.lang.String getManagerName() {
		return this.managerName;
	}

	public void setManagerName(java.lang.String managerName) {
		this.managerName = managerName;
	}

	public java.lang.String getNom() {
		return this.nom;
	}

	public void setNom(java.lang.String nom) {
		this.nom = nom;
	}

	public java.lang.String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(java.lang.String prenom) {
		this.prenom = prenom;
	}

	public java.lang.String getFonction() {
		return this.fonction;
	}

	public void setFonction(java.lang.String fonction) {
		this.fonction = fonction;
	}

	public java.lang.String getService() {
		return this.service;
	}

	public void setService(java.lang.String service) {
		this.service = service;
	}

	public java.lang.String getEntiteJurid() {
		return this.entiteJurid;
	}

	public void setEntiteJurid(java.lang.String entiteJurid) {
		this.entiteJurid = entiteJurid;
	}

	public java.lang.String getCompteDeNom() {
		return this.compteDeNom;
	}

	public void setCompteDeNom(java.lang.String compteDeNom) {
		this.compteDeNom = compteDeNom;
	}

	public CadeauModel(java.lang.String expediteur,
			java.util.Date receptionDate, java.lang.String traitement,
			java.lang.Double valeurEstimee, java.lang.String lien,
			java.lang.String description, java.lang.String commentaire,
			java.lang.String managerName, java.lang.String nom,
			java.lang.String prenom, java.lang.String fonction,
			java.lang.String service, java.lang.String entiteJurid,
			java.lang.String compteDeNom) {
		this.expediteur = expediteur;
		this.receptionDate = receptionDate;
		this.traitement = traitement;
		this.valeurEstimee = valeurEstimee;
		this.lien = lien;
		this.description = description;
		this.commentaire = commentaire;
		this.managerName = managerName;
		this.nom = nom;
		this.prenom = prenom;
		this.fonction = fonction;
		this.service = service;
		this.entiteJurid = entiteJurid;
		this.compteDeNom = compteDeNom;
	}

}