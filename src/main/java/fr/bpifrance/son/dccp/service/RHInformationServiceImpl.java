package fr.bpifrance.son.dccp.service;

import org.kie.api.cdi.KContainer;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieContext;

import fr.bpifrance.son.dccp.model.CadeauModel;

public class RHInformationServiceImpl implements RHInformationService {

	public CadeauModel loadRHInformation(String user) {
		// TODO Auto-generated method stub
		System.out.println("RHInformationServiceImpl.loadRHInformation Appel de loadRHInformation ");
		System.out.println("RHInformationServiceImpl.loadRHInformation user "+user);
		
		
		
		CadeauModel cadeauModel = new CadeauModel();
		cadeauModel.setNom(user);
		cadeauModel.setPrenom(user);
		cadeauModel.setCompteDeNom("Armand DIFFO");
		cadeauModel.setFonction("Analyste");
		cadeauModel.setEntiteJurid("BPI");
		cadeauModel.setManagerName("Raphael VISCOGLIOSI");
		cadeauModel.setService("SON");

		return cadeauModel;
	}

	

}
