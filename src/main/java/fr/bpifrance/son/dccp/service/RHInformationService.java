package fr.bpifrance.son.dccp.service;

import fr.bpifrance.son.dccp.model.CadeauModel;

public interface RHInformationService {

	CadeauModel loadRHInformation(String user);
	
}
